<?php

session_start();

?>

<?php include_once("./layout/header.php"); ?>

<h1>Se connecter</h1>
<form action="/accueil.php" method="post">
    <div>
        <input type="email" name="email" placeholder="Votre email" />
    </div>
    <br>
    <div>
        <input type="password" name="password" placeholder="Mot de passe" />
    </div>
    <br>
    <div>
        <input type="submit" value="CONNEXION" />
    </div>
</form>
<br>

<?php

if( isset($_SESSION['notification']) && count($_SESSION['notification']) > 0 ){

    echo '<em>'.$_SESSION['notification']['msg'].'</em>';
    $_SESSION['notification'] = array();

}

if( isset($_SESSION["user"]) && count($_SESSION["user"]) > 0 ){
    header('Location: accueil.php');
}


?>

<?php include_once("./layout/footer.php"); ?>